//
// Created by Grzesiek on 13.09.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSOutlineView (SelectItem)

/**
* Rozwija listę do wybranej pozycji.
*/
- (void)expandParentsOfItem:(id)item;

/**
* Zaznacza wskazaną pozycję.
*/
- (void)selectItem:(id)item;

@end