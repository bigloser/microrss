//
// Created by Grzesiek on 13.09.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "NSOutlineView+SelectItem.h"


@implementation NSOutlineView (SelectItem)

// ---------------------------------------------------------------------------------------------------------------------
- (void)expandParentsOfItem:(id)item
{
    while (item != nil) {
        id parent = [self parentForItem: item];
        if (![self isExpandable: parent]) {
            break;
        }
        if (![self isItemExpanded: parent]) {
            [self expandItem: parent];
        }
        item = parent;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)selectItem:(id)item
{
    NSInteger itemIndex = [self rowForItem:item];
    if (itemIndex < 0) {
        [self expandParentsOfItem: item];
        itemIndex = [self rowForItem:item];
        if (itemIndex < 0) {
            return;
        }
    }

    [self selectRowIndexes: [NSIndexSet indexSetWithIndex: itemIndex] byExtendingSelection: NO];
}
// ---------------------------------------------------------------------------------------------------------------------

@end