//
//  MGBAddServiceWindowController.h
//  MicroRSS
//
//  Created by Grzesiek on 07.09.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MGBCustomBackgroundView;
@class TinyTinyRSS;

/**
* Kontroler odpowiedzialny za kreator dodawania nowego konta
*/
@interface MGBAddServiceWindowController : NSWindowController

// Pola z formularza
@property (weak) IBOutlet NSTextField *nameField;

@property (weak) IBOutlet NSTextField *loginField;

@property (weak) IBOutlet NSSecureTextField *passwordField;

@property (weak) IBOutlet NSSecureTextField *repeatPasswordField;

@property (weak) IBOutlet NSTextField *urlField;

@property (weak) IBOutlet MGBCustomBackgroundView *errorInfoView;

@property (weak) IBOutlet NSTextField *errorLabel;

@property (weak) IBOutlet NSProgressIndicator *waitSpinner;

/**
* Dane utworzonego serwisu
*/
@property (nonatomic, retain) TinyTinyRSS *createdService;

/**
* Akcja uruchamiana po kliknięciu w guzik dodawania nowego konta
*/
- (IBAction)addAccount:(id)sender;

/**
* Akcja uruchamiana po kliknięciu guzika anuluj
*/
- (IBAction)cancelAddOperation:(id)sender;


@end
