//
//  MGBErrorWindowController.m
//  MicroRSS
//
//  Created by Grzesiek on 03.08.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "MGBErrorWindowController.h"
#import "MGBErrorWindowControllerDelegate.h"

@interface MGBErrorWindowController ()

// Formater używany do wyświetlenia daty powstania błędu
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation MGBErrorWindowController

// ---------------------------------------------------------------------------------------------------------------------
- (void)windowDidLoad
{
    [super windowDidLoad];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    self.dateFormatter = dateFormatter;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)windowWillClose:(NSNotification *)notification
{
    if ([self.delegate respondsToSelector:@selector(errorWindowWillClose)]) {
        [self.delegate errorWindowWillClose];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)setErrorsArray:(NSArray *)errorsArray
{
    _errorsArray = errorsArray;

    [self.errorsTable reloadData];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark - NSTableViewDataSource
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [self.errorsArray count];
}

// ---------------------------------------------------------------------------------------------------------------------
- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSError *error = [self.errorsArray objectAtIndex:row];

    if ([[tableColumn identifier] isEqualToString:@"DateColumn"]) {
        return [self.dateFormatter stringFromDate:[error.userInfo objectForKey:@"MGMCreatedAt"]];
    } else {
        return [error localizedDescription];
    }
}


@end
