//
// Created by Grzesiek on 03.08.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

typedef enum {
    MicroRSSErrorCodeUndefined = 0,
    MicroRSSErrorCodeNotLoggedIn,
    MicroRSSErrorCodeTTRSSError,
    MicroRSSErrorCodeConnectionError
} MicroRSSErrorCode;