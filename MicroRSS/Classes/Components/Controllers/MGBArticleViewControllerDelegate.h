//
// Created by Grzesiek on 04.07.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol MGBArticleViewControllerDelegate <NSObject>

@optional

/**
* Uruchamiane po zmiane zaznaczenia lub oznaczeniu feeda gwiazdką.
*/
- (void)feedStarSelectionChanged:(BOOL)star;

/**
* Uruchamiane po zmianie oznaczenia publikowanego feeda.
*/
- (void)feedPublishSelectionChanged:(BOOL)published;

@required
/**
* Zdarzenie uruchamiane gdy ulegnął zmiane dane feed'ów (np. zostaną one oznaczone jako przeczytane)
*/
- (void)feedsDataChanged:(id)sender;

@end