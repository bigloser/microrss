//
// Created by Grzesiek on 29.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import "ArticleTable.h"
#import "MGBAppDelegate.h"
#import "ArticleTableRowView.h"

@implementation ArticleTable

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)goToWebPage:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(goToWebPage:)]) {
        [self.delegate performSelector:@selector(goToWebPage:) withObject:sender];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)markAsUnread:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(markAsUnread:)]) {
        [self.delegate performSelector:@selector(markAsUnread:) withObject:sender];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)markWithStar:(id)sender
{
    ArticleTableRowView *rowView = [self rowViewAtRow:self.selectedRow makeIfNecessary:NO];

    if (rowView) {
        [rowView starButtonClicked:rowView.starButton];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)publishArticle:(id)sender
{
    ArticleTableRowView *rowView = [self rowViewAtRow:self.selectedRow makeIfNecessary:NO];

    if (rowView) {
        [rowView publishButtonClicked:rowView.publishButton];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)previewArticle:(id)sender
{
    ArticleTableRowView *rowView = [self rowViewAtRow:self.selectedRow makeIfNecessary:NO];

    if (rowView) {
        [rowView previewButtinClicked:nil];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)print:(id)sender
{
    MGBAppDelegate *appDelegate = (MGBAppDelegate *)[[NSApplication sharedApplication] delegate];

    [appDelegate print:sender];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSMenu *)menuForEvent:(NSEvent *)event
{
    NSInteger row = [self rowAtPoint:[self convertPoint: [event locationInWindow] fromView: nil]];
    if (row != -1) {
        [self selectRowIndexes:[NSIndexSet indexSetWithIndex:(NSUInteger)row] byExtendingSelection:NO];
    }

    return [super menuForEvent:event];
}

@end