//
// Created by Grzesiek on 28.07.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ArticleTableRowView.h"


@interface ArticleTableMoreRowView : ArticleTableRowView

/**
* Spinner
*/
@property (nonatomic, assign) IBOutlet NSProgressIndicator *spinner;

@end