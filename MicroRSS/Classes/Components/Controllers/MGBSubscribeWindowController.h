//
//  MGBSubscribeWindowController.h
//  MicroRSS
//
//  Created by Grzesiek on 10.09.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class TinyTinyRSS;
@class MGBCustomBackgroundView;

@interface MGBSubscribeWindowController : NSWindowController

/**
* Serwis do którego został zasubskrybowany feed. Jeśli wciśnięto anuluj
* wartość tego pola będzie równa nil.
*/
@property (nonatomic, retain) TinyTinyRSS *subscribedService;

// Komponenty do wyświetlania błędów
@property (weak) IBOutlet NSTextField *errorLabel;

@property (weak) IBOutlet MGBCustomBackgroundView *errorView;

// Wskaźnik ładowania danych
@property (weak) IBOutlet NSProgressIndicator *loadingProgressIndicator;

// Pola z formularza
@property (weak) IBOutlet NSPopUpButton *servicePopupButton;

@property (weak) IBOutlet NSPopUpButton *categoryPopupButton;

@property (weak) IBOutlet NSTextField *feedURLField;

// Inicjalizator
- (id)initWithServices:(NSArray *)servicesArray selectedServiceID:(NSInteger)selServiceID selectedCategoryID:(NSInteger)selCatID;

// Inicjalizator z dodatkowym parametrem będącym startową wartością feed'a
- (id)initWithServices:(NSArray *)servicesArray selectedServiceID:(NSInteger)selServiceID selectedCategoryID:(NSInteger)selCatID feedURL:(NSString *)url;

// Akcja uruchamiana po zmianie wyboru serwisu
- (IBAction)serviceChanged:(id)sender;

// Akcja uruchamiana po zmianie wyboru kategorii
- (IBAction)categoryChanged:(id)sender;

// Akcje guzików
- (IBAction)subscribeToFeed:(id)sender;

- (IBAction)cancelSubscribeOperation:(id)sender;

@end
