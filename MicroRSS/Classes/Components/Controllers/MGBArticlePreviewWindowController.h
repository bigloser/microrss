//
//  MGBArticlePreviewWindowController.h
//  MicroRSS
//
//  Created by Grzesiek on 26.01.2014.
//  Copyright (c) 2014 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class WebView;

/**
* Kontroler z oknem podglądu artykułów
*/
@interface MGBArticlePreviewWindowController : NSWindowController

/**
* Przeglądarka wyświetlająca podgląd strony
*/
@property (weak) IBOutlet WebView *webView;

/**
* Wczytuje do podglądu dane pochodzące ze wskazanego adresu URL
*/
- (void)loadPreviewForURL:(NSString *)url;
@end
