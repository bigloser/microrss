//
// Created by Grzesiek on 12.09.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "PXSourceList.h"

/**
* Klasa z listą feedów. Główną przyczyną jej istnienia jest nadpisanie funkcji druku.
*/
@interface MGBSourceList : PXSourceList

@end