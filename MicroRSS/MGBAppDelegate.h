//
//  MGBAppDelegate.h
//  MicroRSS
//
//  Created by Grzesiek on 05/24/13.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DataServiceDelegate.h"
#import "PXSourceListDataSource.h"
#import "PXSourceListDelegate.h"
#import "MGBArticleViewControllerDelegate.h"
#import "MGBErrorWindowControllerDelegate.h"
#import "MGBGeneralPrefViewController.h"

@class CNSplitView;
@class PXSourceList;
@class MASPreferencesWindowController;
@class TinyTinyRSS;

@interface MGBAppDelegate : NSObject <NSApplicationDelegate, DataServiceDelegate, PXSourceListDataSource, PXSourceListDelegate,
        MGBArticleViewControllerDelegate, MGBErrorWindowControllerDelegate, MGBGeneralPrefViewControllerDelegate, NSUserNotificationCenterDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSMenuItem *reopenMenuItem;
@property (weak) IBOutlet NSMenuItem *markWithStarMenuItem;
@property (weak) IBOutlet NSMenuItem *publishArticleMenuItem;
@property (weak) IBOutlet CNSplitView *mainSplitView;
@property (weak) IBOutlet PXSourceList *feedsList;
@property (weak) IBOutlet NSView *contentViewContainer;
@property (weak) IBOutlet NSLayoutConstraint *progressBarHeight;
@property (weak) IBOutlet NSProgressIndicator *progressBar;
@property (weak) IBOutlet NSPopover *errorPopover;

// Kontroler okna z preferencjami
@property (nonatomic, retain) MASPreferencesWindowController *preferencesWindowController;

/**
* Zdarzenie wywoływane po zamknięciu okna z dodawaniem nowego konta
*/
- (void)addServiceSheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo;

/**
* Aktualizuje wszystkie feed'y
*/
- (IBAction)updateAll:(id)sender;

/**
* Rozszerza wszystkie feedy
*/
- (IBAction)expandAllFeeds:(id)sender;

/**
* Zwija wszystkie feedy
*/
- (IBAction)collapseAllFeeds:(id)sender;

/**
* Metoda służąca do poinformowania delegata o dodaniu nowego serwisu
*/
- (void)newServiceAdded:(TinyTinyRSS *)service;

/**
* Przeładowuje listę feedów
*/
- (void)serviceRemoved;

/**
* Wyświetla okno z preferencjami
*/
- (IBAction)displayPreferences:(id)sender;

/**
* Oznacza aktualnie zaznaczony feed jako całkowicie przeczytany
*/
- (IBAction)markAllAsRead:(id)sender;

/**
* Wyświetla okno subskrybowania nowego feed'a
*/
- (IBAction)subscribeToFeed:(id)sender;

/**
* Otwiera adres URL feed'a do subskrybcji
*/
- (void)getUrl:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent;

/**
* Zdarzenie uruchamiane po zamknięciu okna z subskrybcją nowego feed'a
*/
- (void)subscribeToFeedSheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(id)contextInfo;

/**
* Zdarzenie uruchamiane przy próbie wypisania się z zaznaczonego kanału
*/
- (IBAction)unsubscribeFeed:(id)sender;

/**
* Zdarzenie uruchamiane po zamknięciu okna z pytaniem o rezygnację z subskrybcji
*/
- (void)unsubscribeAlertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode;

/**
* Akcja drukowania
*/
- (IBAction)print:(id)sender;

/**
* Otwiera ponownie główne okno.
*/
- (IBAction)reopenMainWindow:(id)sender;

/**
* Zdarzenie uruchamiane po zamknięciu głównego okna.
*/
- (IBAction)mainWindowClosed:(id)sender;
@end