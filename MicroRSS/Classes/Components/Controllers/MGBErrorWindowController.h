//
//  MGBErrorWindowController.h
//  MicroRSS
//
//  Created by Grzesiek on 03.08.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol MGBErrorWindowControllerDelegate;

@interface MGBErrorWindowController : NSWindowController <NSWindowDelegate, NSTableViewDataSource>

@property (nonatomic, assign) id<MGBErrorWindowControllerDelegate> delegate;

@property (nonatomic, strong) NSArray *errorsArray;
@property (weak) IBOutlet NSTableView *errorsTable;

@end
