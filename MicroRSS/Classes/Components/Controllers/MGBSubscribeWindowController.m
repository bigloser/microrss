//
//  MGBSubscribeWindowController.m
//  MicroRSS
//
//  Created by Grzesiek on 10.09.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "MGBSubscribeWindowController.h"
#import "TinyTinyRSS.h"
#import "TinyTinyRSSFeed.h"
#import "MGBCustomBackgroundView.h"

@interface MGBSubscribeWindowController ()

// tablica z dostępnymi serwisami
@property (nonatomic, retain) NSArray *services;

// numer ID zaznaczonego serwisu
@property (nonatomic, assign) NSInteger selectedServiceID;

// numer id zaznaczonej kategorii
@property (nonatomic, assign) NSInteger selectedCategoryID;

// Początkowy adres URL jaki będzie wpisany w pole
@property (nonatomic, retain) NSString *startingURL;

// przeładowuje listę kategorii
- (void)reloadCategoryMenu;
@end

@implementation MGBSubscribeWindowController

// ---------------------------------------------------------------------------------------------------------------------
- (id)initWithServices:(NSArray *)servicesArray selectedServiceID:(NSInteger)selServiceID selectedCategoryID:(NSInteger)selCatID
{
    self = [super initWithWindowNibName:@"MGBSubscribeWindow"];

    if (self) {
        self.services           = servicesArray;
        self.selectedServiceID  = selServiceID;
        self.selectedCategoryID = selCatID;
    }

    return self;
}

- (id)initWithServices:(NSArray *)servicesArray selectedServiceID:(NSInteger)selServiceID selectedCategoryID:(NSInteger)selCatID feedURL:(NSString *)url
{
    self = [self initWithServices:servicesArray selectedServiceID:selServiceID selectedCategoryID:selCatID];

    if (self) {
        self.startingURL = url;
    }

    return self;
}


// ---------------------------------------------------------------------------------------------------------------------
- (void)windowDidLoad
{
    [super windowDidLoad];

    self.errorView.backgroundRenderer = ^(NSRect dirtyRect, MGBCustomBackgroundView *view) {
        //// Color Declarations
        NSColor* borderColor = [NSColor colorWithCalibratedRed: 0.713 green: 0 blue: 0 alpha: 1];
        NSColor* backgroundColor = [NSColor colorWithCalibratedRed: 0.993 green: 0.783 blue: 0.783 alpha: 1];

        //// Rectangle Drawing
        NSBezierPath* rectanglePath = [NSBezierPath bezierPathWithRect: view.bounds];
        [backgroundColor setFill];
        [rectanglePath fill];
        [borderColor setStroke];
        [rectanglePath setLineWidth: 1];
        [rectanglePath stroke];
    };

    if (self.startingURL) {
        self.feedURLField.stringValue = self.startingURL;
        self.startingURL = nil; // nie będzie już potrzebny
    }

    NSMenuItem *selectedMenuItem;
    for (TinyTinyRSS *service in self.services) {
        NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:service.name action:nil keyEquivalent:@""];

        item.tag = service.serviceID;
        [self.servicePopupButton.menu addItem:item];

        if (service.serviceID == self.selectedServiceID) {
            selectedMenuItem = item;
        }
    }

    if (selectedMenuItem) {
        [self.servicePopupButton selectItem:selectedMenuItem];
    } else {
        selectedMenuItem = [self.servicePopupButton selectedItem];

        self.selectedServiceID = selectedMenuItem.tag;
    }

    [self reloadCategoryMenu];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)reloadCategoryMenu
{
    [self.categoryPopupButton.menu removeAllItems];

    NSMenuItem *noCategoryMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"(Uncategorized)", @"(Bez kategorii)") action:nil keyEquivalent:@""];
    noCategoryMenuItem.tag = 0;

    [self.categoryPopupButton.menu addItem:noCategoryMenuItem];

    if (self.selectedServiceID) {
        TinyTinyRSS *selectedService;
        for (TinyTinyRSS *service in self.services) {
            if (service.serviceID == self.selectedServiceID) {
                selectedService = service;
            }
        }

        if (selectedService) {
            NSMenuItem *selectedCategoryItem;
            for (id categoryIndex in selectedService.categoryIndex) {
                TinyTinyRSSFeed *category = [selectedService.categoryIndex objectForKey:categoryIndex];

                NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:category.name action:nil keyEquivalent:@""];
                item.tag = category.feedID;

                [self.categoryPopupButton.menu addItem:item];

                if (category.feedID == self.selectedCategoryID) {
                    selectedCategoryItem = item;
                }
            }

            if (selectedCategoryItem) {
                [self.categoryPopupButton selectItem:selectedCategoryItem];
            } else {
                self.selectedCategoryID = 0;
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)subscribeToFeed:(id)sender
{
    [self.errorView setHidden:YES];

    NSURL *feedURL = [NSURL URLWithString:self.feedURLField.stringValue];

    if (feedURL && feedURL.scheme && feedURL.host) {
        TinyTinyRSS *selectedService;
        for (TinyTinyRSS *service in self.services) {
            if (service.serviceID == self.selectedServiceID) {
                selectedService = service;
            }
        }

        if (!selectedService) {
            // błąd
            return;
        }

        [self.loadingProgressIndicator startAnimation:self];

        [selectedService subscribeToFeed:self.feedURLField.stringValue forCategory:self.selectedCategoryID callback:^(NSError *error) {
            [self.loadingProgressIndicator stopAnimation:self];

            if (error) {
                self.errorLabel.stringValue = [error localizedDescription];
                [self.errorView setHidden:NO];
            } else {
                self.subscribedService = selectedService;

                [NSApp endSheet:self.window];
            }
        }];
    } else {
        self.errorLabel.stringValue = NSLocalizedString(@"URL address is invalid.", @"Adres URL jest nieprawidłowy.");
        [self.errorView setHidden:NO];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)cancelSubscribeOperation:(id)sender
{
    [NSApp endSheet:self.window];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)serviceChanged:(id)sender
{
    self.selectedServiceID = self.servicePopupButton.selectedItem.tag;

    [self reloadCategoryMenu];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)categoryChanged:(id)sender
{
    self.selectedCategoryID = self.categoryPopupButton.selectedItem.tag;
}

// ---------------------------------------------------------------------------------------------------------------------

@end
