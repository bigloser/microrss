//
// Created by Grzesiek on 10.07.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <FMDB/FMDatabaseQueue.h>
#import <FMDB/FMDatabase.h>
#import "MicroRSSArticleTable.h"
#import "TinyTinyRSSFeed.h"
#import "MicroRSSDatabase.h"
#import "TinyTinyRSSArticle.h"
#import "TinyTinyRSS.h"


@implementation MicroRSSArticleTable

// ---------------------------------------------------------------------------------------------------------------------
+ (void)updateTTRSSArticles:(NSArray *)articles forFeed:(TinyTinyRSSFeed *)feed append:(BOOL)appendArticles
{
    if (!feed.dbID) {
        // jeśli jakimś cudem feed nie ma swojego ID w bazie to nie mamy co tu robić
        // ale to się nigdy nie zdarzy (a przynajmniej nie powinno)
        return;
    }

    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSNumber *feedDBID = [[NSNumber alloc] initWithInteger:feed.dbID];

        if (!appendArticles) {
            [db executeUpdate:@"DELETE FROM default_article WHERE feed_db_id = ?", feedDBID]; // kasowanie staroci
        }

        NSNumber *serviceID = [[NSNumber alloc] initWithInteger:feed.service.serviceID];

        for (TinyTinyRSSArticle *article in articles) {
            NSNumber *articleID = [[NSNumber alloc] initWithInteger:article.articleID];
            NSNumber *feedID    = [[NSNumber alloc] initWithInteger:article.feedID];
            NSNumber *unread    = [[NSNumber alloc] initWithBool:article.unread];
            NSNumber *starred   = [[NSNumber alloc] initWithBool:article.starred];
            NSNumber *published = [[NSNumber alloc] initWithBool:article.published];
            NSNumber *updated   = [[NSNumber alloc] initWithInteger:article.updated];

            [db executeUpdate:@"INSERT INTO default_article (feed_db_id, service_id, feed_id, article_id, title, unread, starred, "
                                      "published, link, excerpt, content, updated) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                            feedDBID,
                            serviceID,
                            feedID,
                            articleID,
                            article.articleTitle,
                            unread,
                            starred,
                            published,
                            article.link,
                            article.excerpt,
                            article.content,
                            updated
            ];

            if ([db hadError]) {
                NSLog(@"%@", [db lastError]);
            }

            article.dbID = (NSInteger)[db lastInsertRowId];
        }
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)addNewTTRSSArticles:(NSArray *)articles forFeed:(TinyTinyRSSFeed *)feed
{
    if (!feed.dbID) {
        // jeśli jakimś cudem feed nie ma swojego ID w bazie to nie mamy co tu robić
        // ale to się nigdy nie zdarzy (a przynajmniej nie powinno)
        return;
    }

    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSNumber *feedDBID = [[NSNumber alloc] initWithInteger:feed.dbID];
        NSNumber *count    = [[NSNumber alloc] initWithUnsignedInteger:articles.count];

        [db executeUpdate:@"DELETE FROM default_article WHERE id IN (SELECT id FROM default_article WHERE feed_db_id = ? ORDER BY updated ASC LIMIT ?)", feedDBID, count];

        if ([db hadError]) {
            NSLog(@"%@", [db lastError]);
        }

        NSNumber *serviceID = [[NSNumber alloc] initWithInteger:feed.service.serviceID];

        for (TinyTinyRSSArticle *article in articles) {
            NSNumber *articleID = [[NSNumber alloc] initWithInteger:article.articleID];
            NSNumber *feedID    = [[NSNumber alloc] initWithInteger:article.feedID];
            NSNumber *unread    = [[NSNumber alloc] initWithBool:article.unread];
            NSNumber *starred   = [[NSNumber alloc] initWithBool:article.starred];
            NSNumber *published = [[NSNumber alloc] initWithBool:article.published];
            NSNumber *updated   = [[NSNumber alloc] initWithInteger:article.updated];

            [db executeUpdate:@"INSERT INTO default_article (feed_db_id, service_id, feed_id, article_id, title, unread, starred, "
                                      "published, link, excerpt, content, updated) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
                              feedDBID,
                              serviceID,
                              feedID,
                              articleID,
                              article.articleTitle,
                              unread,
                              starred,
                              published,
                              article.link,
                              article.excerpt,
                              article.content,
                              updated
            ];

            if ([db hadError]) {
                NSLog(@"%@", [db lastError]);
            }

            article.dbID = (NSInteger)[db lastInsertRowId];
        }
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSMutableArray *)fetchTTRSSArticlesForFeed:(TinyTinyRSSFeed *)feed andService:(TinyTinyRSS *)service
{
    if (!feed.dbID) {
        // ID z bazy musi być obecne (jeśli go nie ma to coś jest nie tak)
        return nil;
    }

    FMDatabase *db = [[FMDatabase alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    if (![db open]) {
        return nil;
    }

    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSNumber *feedDBID    = [[NSNumber alloc] initWithInteger:feed.dbID];

    NSMutableString *queryString = [[NSMutableString alloc] initWithString:@"SELECT * FROM default_article WHERE feed_db_id = ?"];
    if (feed.articlesVisibility == MGBArticleVisibilityUnread) {
        [queryString appendString:@" AND unread = 1"];
    }

    if (feed.articlesSortOrder == MGBSortingOrderAscending) {
        [queryString appendString:@" ORDER BY updated ASC"];
    } else {
        [queryString appendString:@" ORDER BY updated DESC"];
    }

    FMResultSet *resultSet = [db executeQuery:queryString, feedDBID];

    while ([resultSet next]) {
        TinyTinyRSSArticle *article = [[TinyTinyRSSArticle alloc] initWithResultSet:resultSet];

        if (article.feedID == feed.feedID) {
            article.feed = feed;
        } else {
            // wyszukiwanie feeda do którego należy dany artykuł
            NSNumber *feedIDKey = [[NSNumber alloc] initWithInteger:article.feedID];
            TinyTinyRSSFeed *parentFeed = [service.feedsIndex objectForKey:feedIDKey];

            if (parentFeed) {
                article.feed = parentFeed;
            } else {
                article.feed = feed;
            }
        }

        [array addObject:article];
    }

    return array;
}

// ---------------------------------------------------------------------------------------------------------------------
+ (int)topArticleIDForFeedID:(NSInteger)feedDBID
{
    FMDatabase *db = [[FMDatabase alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    if (![db open]) {
        return 0;
    }

    NSNumber *feedID = [[NSNumber alloc] initWithInteger:feedDBID];

    FMResultSet *resultSet = [db executeQuery:@"SELECT article_id FROM default_article WHERE feed_db_id = ? ORDER BY article_id DESC", feedID];

    if ([resultSet next]) {
        return [resultSet intForColumn:@"article_id"];
    } else {
        return 0;
    }
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)markArticle:(NSInteger)articleID forService:(NSInteger)serviceID asUnread:(BOOL)unread
{
    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSNumber *articleNumberID = [[NSNumber alloc] initWithInteger:articleID];
        NSNumber *serviceNumberID = [[NSNumber alloc] initWithInteger:serviceID];
        NSNumber *isUnreadNumber  = [[NSNumber alloc] initWithBool:unread];

        [db executeUpdate:@"UPDATE default_article SET unread = ? WHERE service_id = ? AND article_id = ?", isUnreadNumber, serviceNumberID, articleNumberID];
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)markArticle:(NSInteger)articleID forService:(NSInteger)serviceID asStarred:(BOOL)starred
{
    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSNumber *articleNumberID = [[NSNumber alloc] initWithInteger:articleID];
        NSNumber *serviceNumberID = [[NSNumber alloc] initWithInteger:serviceID];
        NSNumber *isStarredNumber = [[NSNumber alloc] initWithBool:starred];

        [db executeUpdate:@"UPDATE default_article SET starred = ? WHERE service_id = ? AND article_id = ?", isStarredNumber, serviceNumberID, articleNumberID];
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)markArticle:(NSInteger)articleID forService:(NSInteger)serviceID asPublished:(BOOL)published
{
    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSNumber *articleNumberID   = [[NSNumber alloc] initWithInteger:articleID];
        NSNumber *serviceNumberID   = [[NSNumber alloc] initWithInteger:serviceID];
        NSNumber *isPublishedNumber = [[NSNumber alloc] initWithBool:published];

        [db executeUpdate:@"UPDATE default_article SET published = ? WHERE service_id = ? AND article_id = ?", isPublishedNumber, serviceNumberID, articleNumberID];
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (void)saveArticleMaxID:(NSInteger)articleID forFeed:(NSInteger)feedDBID
{
    FMDatabaseQueue *queue = [[FMDatabaseQueue alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    [queue inDatabase:^(FMDatabase *db) {
        NSNumber *articleNumberID   = [[NSNumber alloc] initWithInteger:articleID];
        NSNumber *feedID            = [[NSNumber alloc] initWithInteger:feedDBID];

        [db executeUpdate:@"DELETE FROM default_article_max_id WHERE id = ?", feedID];
        [db executeUpdate:@"INSERT INTO default_article_max_id VALUES (?, ?)", feedID, articleNumberID];
    }];
}

// ---------------------------------------------------------------------------------------------------------------------
+ (NSInteger)articleMaxID:(NSInteger)feedDBID
{
    FMDatabase *db = [[FMDatabase alloc] initWithPath:[MicroRSSDatabase databasePathString]];

    if (![db open]) {
        return 0;
    }

    NSNumber *feedID       = [[NSNumber alloc] initWithInteger:feedDBID];
    FMResultSet *resultSet = [db executeQuery:@"SELECT value FROM default_article_max_id WHERE id = ? LIMIT 1", feedID];

    if ([resultSet next]) {
        return [resultSet intForColumnIndex:0];
    }

    return 0;
}

// ---------------------------------------------------------------------------------------------------------------------

@end