//
// Created by Grzesiek on 28.05.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

/**
* Klasa bazowa dla wszystkich elementów które można wyświetlić na drzewku.
*/
@interface DataServiceTreeItem : NSObject

/**
* Czy ma odznakę z listą nieprzeczytanych elementów
*/
- (BOOL)hasBadge;

/**
* Wartość wyświetlana w odznace
*/
- (NSInteger)badgeValue;

/**
* Tytuł elementu na liście
*/
- (NSString *)title;

/**
* Liczba elementów potomnych
*/
- (NSUInteger)numberOfChildren;

/**
* Elementy potomne
*/
- (NSArray *)children;

/**
* Czy dana pozycja może być rozwinięta
*/
- (BOOL)isExpandable;

/**
* Czy element ma ikonę
*/
- (BOOL)hasIcon;

/**
* Okona elementu
*/
- (NSImage *)icon;

/**
* Pobiera nagłówki z serwera.
*/
- (void)downloadArticles:(void (^)(NSMutableArray *))callback;

/**
* Pobiera więcej artykułów z serwera.
*/
- (void)downloadMoreArticles:(void (^)(NSArray *))callback startFrom:(NSUInteger)start;

/**
* Anuluje pobierane artykułów jeśli jakieś jest w trakcie.
*/
- (void)cancelArticleDownload;

/**
* Sprwadza czy dane artykułów mogły ulec zmianie
*/
- (BOOL)isArticleDataChanged;

/**
* Zapisuje zmiany
*/
- (void)commit;

/**
* Usuwa serwis z bazy
*/
- (void)removeFromDB;

@end