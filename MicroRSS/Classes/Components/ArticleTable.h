//
// Created by Grzesiek on 29.06.2013.
// Copyright (c) 2013 mobilegb.eu. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface ArticleTable : NSTableView

/**
* Akcja odpalana z menu powodująca przejście do artykułu
* Jest przekazywana wyżej do delegata w celu obsłużenia.
*/
- (IBAction)goToWebPage:(id)sender;

/**
* Akcja odpalana z menu i pozwalająca na oznaczenie artykułu jako nieprzeczytany.
* Jest przekazywana wyżej do delegata w celu obsłużenia.
*/
- (IBAction)markAsUnread:(id)sender;

/**
* Wymusza kliknięcie guzika gwiazdki w aktualnie zaznaczonym wierszu.
*/
- (IBAction)markWithStar:(id)sender;

/**
* Wymusza kliknięcie guzika publikacji w aktualnie zaznaczonym wierszu.
*/
- (IBAction)publishArticle:(id)sender;

/**
* Otwiera podgląd wybranego artykułu
*/
- (IBAction)previewArticle:(id)sender;
@end