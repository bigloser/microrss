//
//  MGBAccountsPrefViewController.m
//  MicroRSS
//
//  Created by Grzesiek on 31.08.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import "MGBAccountsPrefViewController.h"
#import "TinyTinyRSS.h"
#import "MGBAppDelegate.h"
#import "MGBAddServiceWindowController.h"
#import "MASPreferencesWindowController.h"

@interface MGBAccountsPrefViewController ()

// Aktualnie zaznaczony (edytowany) serwis
@property (nonatomic, retain) TinyTinyRSS *selectedService;

// Kontroler odpowiedzialny za dodawanie nowych serwisów
@property (nonatomic, retain) MGBAddServiceWindowController *addServiceWindowController;

@end

@implementation MGBAccountsPrefViewController

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)identifier
{
    return @"Accounts";
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSImage *)toolbarItemImage
{
    return [NSImage imageNamed:NSImageNameUserAccounts];
}

// ---------------------------------------------------------------------------------------------------------------------
- (NSString *)toolbarItemLabel
{
    return NSLocalizedString(@"Accounts", @"Etykieta dla panelu preferencji zarządzania kontami");
}

// ---------------------------------------------------------------------------------------------------------------------
- (BOOL)commitEditing
{
    if (self.selectedService) {
        if (self.nameField.stringValue.length > 0) {
            self.selectedService.name = self.nameField.stringValue;
        }
        if (self.loginField.stringValue.length > 0) {
            self.selectedService.login = self.loginField.stringValue;
        }
        if (self.passwordField.stringValue.length > 0) {
            self.selectedService.password = self.passwordField.stringValue;
        }

        NSMutableString *apiUrl = [[NSMutableString alloc] initWithString:[self.urlField.stringValue lowercaseString]];

        if (apiUrl.length > 8) {
            // dodawanie do adresu URL części /api/ jeśli jej nie wprowadzono
            if ([apiUrl characterAtIndex:apiUrl.length -1] != '/') {
                [apiUrl appendString:@"/"];
            }

            // sprwdzanie czy adres URL kończy się na /api/
            if (![[apiUrl substringFromIndex:apiUrl.length -5] isEqualToString:@"/api/"]) {
                [apiUrl appendString:@"api/"];
            }

            // upewnianie się że adres URL rozpoczyna się od http lub https
            if (![[apiUrl substringToIndex:7] isEqualToString:@"http://"] && ![[apiUrl substringToIndex:8] isEqualToString:@"https://"]) {
                self.selectedService.apiURL = [[NSURL alloc] initWithString:[@"http://" stringByAppendingString:apiUrl]];
            } else {
                self.selectedService.apiURL = [[NSURL alloc] initWithString:apiUrl];
            }
        }

        [self.selectedService commit];
    }

    return [super commitEditing];
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)showAddWindow:(id)sender
{
    MGBAppDelegate *appDelegate = (MGBAppDelegate *)[[NSApplication sharedApplication] delegate];

    if (!self.addServiceWindowController) {
        MGBAddServiceWindowController *addController = [[MGBAddServiceWindowController alloc] initWithWindowNibName:@"MGBAddServiceWindow"];

        self.addServiceWindowController = addController;
    }

    [NSApp beginSheet:self.addServiceWindowController.window modalForWindow:appDelegate.preferencesWindowController.window
        modalDelegate:self
       didEndSelector:@selector(sheetDidEnd:returnCode:contextInfo:)
          contextInfo:nil];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(NSInteger)returnCode contextInfo:(void*)contextInfo
{
    [sheet orderOut:self];

    if (self.addServiceWindowController.createdService) {
        [self.servicesArray addObject:self.addServiceWindowController.createdService];

        MGBAppDelegate *appDelegate = (MGBAppDelegate *)[[NSApplication sharedApplication] delegate];
        [appDelegate newServiceAdded:self.addServiceWindowController.createdService];

        [self.accountsTable reloadData];
    }

    self.addServiceWindowController = nil;
}

// ---------------------------------------------------------------------------------------------------------------------
- (IBAction)deleteSelectedAccount:(id)sender
{
    if (!self.selectedService) {
        return;
    }

    MGBAppDelegate *appDelegate = (MGBAppDelegate *)[[NSApplication sharedApplication] delegate];

    NSAlert *alert = [[NSAlert alloc] init];

    [alert addButtonWithTitle:NSLocalizedString(@"Delete", @"Usuń")];
    [alert addButtonWithTitle:NSLocalizedString(@"Cancel", @"Anuluj")];
    [alert setMessageText:[NSString stringWithFormat:NSLocalizedString(@"Delete %@?", @"Usunąć %@?"), self.selectedService.name]];
    [alert setInformativeText:NSLocalizedString(@"Are you sure to delete this account?", @"Czy na pewno usunąć to konto?")];
    [alert setAlertStyle:NSInformationalAlertStyle];

    [alert beginSheetModalForWindow:appDelegate.preferencesWindowController.window modalDelegate:self didEndSelector:@selector(deleteAlertDidEnd:returnCode:) contextInfo:nil];
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)deleteAlertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode
{
    if (returnCode == NSAlertFirstButtonReturn) {
        [self.servicesArray removeObject:self.selectedService];

        self.nameField.stringValue     = @"";
        self.loginField.stringValue    = @"";
        self.passwordField.stringValue = @"";
        self.urlField.stringValue      = @"";

        [self.selectedService removeFromDB];

        [self.accountsTable reloadData];

        MGBAppDelegate *appDelegate = (MGBAppDelegate *)[[NSApplication sharedApplication] delegate];
        [appDelegate serviceRemoved];
    }
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark NSTableViewDataSource
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [self.servicesArray count];
}

// ---------------------------------------------------------------------------------------------------------------------
#pragma mark NSTableViewDelegate
- (NSTableRowView *)tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row
{
    NSTableRowView *rowView = [tableView makeViewWithIdentifier:@"AccountRowViewKey" owner:self];

    NSTextField *nameLabel = [rowView viewWithTag:1];
    NSTextField *urlLabel  = [rowView viewWithTag:2];

    TinyTinyRSS *service = [self.servicesArray objectAtIndex:(NSUInteger)row];

    nameLabel.stringValue = service.name;
    urlLabel.stringValue  = [service.apiURL absoluteString];

    return rowView;
}

// ---------------------------------------------------------------------------------------------------------------------
- (void)tableViewSelectionDidChange:(NSNotification *)notification
{
    [self commitEditing];

    NSInteger selectedRow = [self.accountsTable selectedRow];

    if (selectedRow > -1) {
        TinyTinyRSS *service = [self.servicesArray objectAtIndex:(NSUInteger)selectedRow];

        self.nameField.stringValue     = service.name;
        self.loginField.stringValue    = service.login;
        self.passwordField.stringValue = service.password;
        self.urlField.stringValue      = [service.apiURL absoluteString];

        self.selectedService = service;
    } else {
        self.nameField.stringValue     = @"";
        self.loginField.stringValue    = @"";
        self.passwordField.stringValue = @"";
        self.urlField.stringValue      = @"";

        self.selectedService = nil;
    }
}

@end
