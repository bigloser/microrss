//
//  MGBGeneralPrefViewController.h
//  MicroRSS
//
//  Created by Grzesiek on 31.08.2013.
//  Copyright (c) 2013 mobilegb.eu. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MASPreferencesViewController.h"

@protocol MGBGeneralPrefViewControllerDelegate;

/**
* Kontroler odpowiedzialny za edycję ogólnych preferencji programu
*/
@interface MGBGeneralPrefViewController : NSViewController <MASPreferencesViewController>

/**
* Pole z częstotliwością odświeżania
*/
@property (weak) IBOutlet NSTextField *refreshRateField;

/**
* Select z rodzajem sortowania
*/
@property (weak) IBOutlet NSPopUpButton *sortingOrderSelect;

/**
 * Select z wyborem sposobu wyświetlania daty na liście artykułów
 */
@property (weak) IBOutlet NSPopUpButton *articleDateDisplayMethodSelect;

/**
 * Select z wyborem sposobu wyświetlania feed'ów
 */
@property (weak) IBOutlet NSPopUpButton *feedsVisibilitySelect;

/**
 * Checkbox przełączający opcję wyświetlania nazwy feed'ów na liście artykułów
 */
@property (weak) IBOutlet NSButton *showFeedsTitleCheckbox;

/**
* Guzik wyboru layoutu
*/
@property (weak) IBOutlet NSPopUpButton *layoutSelect;

/**
* Checkbox pozwalający na ustawienie akceptacji samo podpisanych certyfikatów
*/
@property (weak) IBOutlet NSButton *acceptSelfSignedCertCheckbox;

/**
* Delegat który zostanie poinformowany o zmianie ustawień
*/
@property (nonatomic, weak) id<MGBGeneralPrefViewControllerDelegate> delegate;

@end

@protocol MGBGeneralPrefViewControllerDelegate

@required
/**
* Metoda wywoływana po zapisaniu ustawień
*/
- (void)generalPreferencesCommited:(NSDictionary *)preferences;

@end
